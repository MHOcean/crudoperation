Rails.application.routes.draw do
  resources :infos
  get 'infos/index'
  get 'infos/show'
  get 'infos/new'
  get 'infos/create'
  get 'infos/edit'
  get 'infos/update'
  get 'infos/destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
