class AddPasswordToInfos < ActiveRecord::Migration[5.2]
  def change
    add_column :infos, :password, :string
  end
end
