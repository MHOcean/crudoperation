class CreateInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :infos do |t|
      t.string :name
      t.integer :age
      t.string :email
      t.text :bio

      t.timestamps
    end
  end
end
