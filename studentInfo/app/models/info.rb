class Info < ApplicationRecord
  validates :name, :presence => true
  validates :email, :presence => true
  validates :password, :confirmation => true
  validates :password_confirmation, :presence => true
end
