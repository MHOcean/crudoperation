class InfosController < ApplicationController
  def index
    @infos = Info.all
    render :index
  end

  def show
    @info = Info.find(params[:id])
    render :show
  end

  def new
    @info = Info.new
    render :new
  end

  def create
    @info = Info.new(info_params)
    if @info.save
      redirect_to infos_path
    else
      render :new
    end
  end
  def edit
    @info = Info.find(params[:id])
    render :edit
  end

  def update
    @info = Info.find(params[:id])
    if @info.update(info_params)
      redirect_to infos_path
    else
      render :edit
    end
  end

  def destroy
    @info = Info.find(params[:id])
    @info.destroy
    redirect_to infos_path
  end
  private
  def info_params
    params.require(:info).permit(:name,:age,:email,:password,:password_confirmation,:bio)
  end
end
